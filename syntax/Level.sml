structure Level :> LEVEL =
struct
   datatype t = Stable
              | Changeable
              | LVar of LvVar.LvVar
              | Unknown
              | Undetermined of LExp.lexp

   fun toString lv = 
     case lv of
       Stable => "$S"
     | Changeable => "$C"
     | LVar v => LvVar.toString v
     | Unknown => "$?" 
     | Undetermined v => LExp.lexpToString v

end
