
structure Translate : TRANSLATE =
struct
    (* Import *)
    open LVType
    open SMLProgram
    infixr ^^ ^/^
    val error = Error.error
    fun display f = (PrettyPrint.output (TextIO.stdOut, f, 79);
                   TextIO.flushOut TextIO.stdOut)

    type Lsigma = (var list * Constraint.constraint)

    fun loopOpt f opt = case opt of
                          NONE => ()
                        | SOME opt => f opt
    val copy = Source.copy

    (* SAC Primitive *)
    val vidRead = VId.fromString "read"
    val vidWrite = VId.fromString "write MLton.eq"
    val vidMod = VId.fromString "modref"

    val tyMod = LongTyCon.fromId (TyCon.fromString "mod")
    
    fun LongVIdExp(I, longvid) = ATExp(copy I, IDAtExp(copy I, SANSOp, longvid))
    fun LongVIdPat(I, longvid) = ATPat(copy I, IDAtPat(copy I, SANSOp, longvid))

    fun VIdAtExp(I, vid) = IDAtExp(copy I, SANSOp, LongVId.fromId vid)
    fun VIdAtPat(I, vid) = IDAtPat(copy I, SANSOp, LongVId.fromId vid)
    fun VIdExp(I, vid) = LongVIdExp(I, LongVId.fromId vid)
    fun VIdPat(I, vid) = LongVIdPat(I, LongVId.fromId vid)

    fun ExpApp(I, vid, atexp) = APPExp(I, VIdExp(I, vid), atexp)
    fun AtExpApp(I, vid, atexp) = PARAtExp(I, ExpApp(I, vid, atexp))
    fun ExpRead(I, atexp, exp) = 
      let val pair = DerivedFormsCore.TUPLEAtExp(I, [ATExp(I,atexp), exp])
      in ExpApp(I, vidRead, pair) end
    fun ExpFn(I, vid, exp) = 
         FNExp(I, Match(I, Mrule(I, VIdPat(I,vid), exp), NONE))
    fun ExpReadWrite(I, atexp) = 
      let val vid = VId.invent()
      in ExpRead(I, atexp, ExpFn(I,vid,ExpApp(I,vidWrite,VIdAtExp(I,vid)))) end

    (* Core *)

   (* Expressions *)

    fun loopAtExp (mode, atexp) =
      let
        val tau = getType(infoAtExp atexp)
      in
        case atexp of
          SCONAtExp(I, scon) => 
            (case mode of  (* TODO *)
               Level.Changeable => AtExpApp(I, vidWrite, atexp) 
             | _ => atexp
            )
        | IDAtExp(I, _, longvid) =>
            (case getKind I of
               IdStatus.v =>
                 (case mode of
                    Level.Changeable => 
                      if !(getLv tau) = Level.Changeable
                      then PARAtExp(I,ExpReadWrite(I, atexp))
                      else AtExpApp(I, vidWrite, atexp)
                  | _ => atexp)
             | IdStatus.c => 
                 (case mode of
                    Level.Changeable => AtExpApp(I, vidWrite, atexp)
                  | _ =>
                      if !(getLv tau) = Level.Changeable
                      then AtExpApp(I, vidMod, AtExpApp(I, vidWrite, atexp))
                      else atexp)
             | IdStatus.e => atexp)
        | RECORDAtExp(I, exprow_opt) => let
            val exprow_opt = Option.map (fn row => loopExpRow(mode,row)) exprow_opt
            val atexp = RECORDAtExp(I, exprow_opt)
          in
            case mode of
              Level.Changeable => AtExpApp(I, vidWrite, atexp)
            | _ => atexp
          end
        | LETAtExp(I, dec, exp) => let
            val dec = loopDec dec
            val exp = loopExp (mode, exp)
          in
            LETAtExp(I, dec, exp)
          end
        | PARAtExp(I, exp) => let
            val exp = loopExp (mode, exp)
          in
            PARAtExp(I, exp)
          end
      end

    and loopExpRow (mode, ExpRow(I, lab, exp, exprow_opt)) =
        let
          val exp = loopExp (Level.Stable, exp)
          val exprow_opt = Option.map (fn row => loopExpRow(mode,row)) exprow_opt
        in
          ExpRow(I, lab, exp, exprow_opt)
        end

    and loopExp (mode, exp) =
          case exp of
            (ATExp(I, atexp)) => let
              val atexp = loopAtExp (mode, atexp)
            in
              ATExp(I, atexp)
            end
          | (APPExp(I, exp, atexp)) => 
              let
                val isCon = AST.isCon exp
                val argTy = getType (infoAtExp atexp)
                val f = getType(infoExp exp)
                val FunType (_,tau2,fmode,lv) = !f
                val exp = loopExp (Level.Stable, exp)
                val atexp = loopAtExp (Level.Stable, atexp)
                val app = if AST.isCase exp 
                          then if !(getLv argTy) = Level.Changeable then
                                 ExpRead(I, atexp, exp)
                               else 
                                 APPExp(I, exp, atexp)
                          else APPExp(I, exp, atexp)
              in
                if isCon then
                  case (mode, !(getLv tau2)) of
                    (Level.Stable, Level.Stable) => app
                  | (Level.Stable, Level.Changeable) => ExpApp(I, vidMod, AtExpApp(I, vidWrite, PARAtExp(I,app)))
                  | (Level.Changeable, _) => ExpApp(I, vidWrite, PARAtExp(I,app))
                  | _ => Error.error(I,"not possible")
                else
                  case (!fmode, !lv, mode) of
                    (Level.Stable, Level.Stable, Level.Stable) => app
                  | (Level.Changeable, Level.Stable, Level.Changeable) => app
                  | (Level.Stable, Level.Stable, Level.Changeable) => 
                      if !(getLv tau2) = Level.Stable 
                      then ExpApp(I, vidWrite, PARAtExp(I, app))
                      else ExpReadWrite (I, PARAtExp(I,app))
                  | (_, Level.Changeable, Level.Changeable) =>
                      app (* TODO *)
                  | _ => ExpApp(I, vidMod, PARAtExp(I,app))
              end
          | (COLONExp(I, exp, ty)) => 
              let 
                val exp = loopExp (mode,exp)
                val ty = loopTy ty
              in
                COLONExp(I, exp, ty)
              end
          | (HANDLEExp(I, exp, match)) => 
              let
                val exp = loopExp (mode,exp)
                val match = loopMatch (mode,match) 
              in
                HANDLEExp(I, exp, match)
              end
          | (RAISEExp(I, exp)) => 
              let val exp = loopExp (mode,exp)
              in RAISEExp(I, exp) end
          | (FNExp(I, match)) => 
              let 
                val match = loopMatch (mode,match)
              in 
                FNExp(I, match)
              end


    (* Matches *)

    and loopMatch (mode, Match(I, mrule, match_opt)) = 
        let
          val mrule = loopMrule (mode,mrule)
          val match_opt = Option.map (fn m => loopMatch(mode,m)) match_opt
        in
          Match(I, mrule, match_opt)
        end
    and loopMrule (mode, Mrule(I, pat, exp)) =  (* Fun/Case *)
        let
          val tau = getType I
          val FunType(_,_,fmode,_) = !tau

          val pat = loopPat pat 
          val exp = loopExp (!fmode, exp)
        in
          case mode of
            Level.Changeable => let
              val exp = ExpApp(I, vidWrite, PARAtExp (I,exp))
              in Mrule(I, pat, exp) end
          | _ => Mrule(I, pat, exp)
        end

    (* Declarations *)

    and loopDec dec =
         case dec of
           (VALDec(I, tyvarseq, valbind)) =>
             let
               val valbind = loopValBind valbind
             in
               VALDec(I, tyvarseq, valbind)      
             end
         | (TYPEDec(I, typbind)) => 
             let val typbind = loopTypBind typbind
             in TYPEDec(I, typbind) end
         | (DATATYPEDec(I, datbind)) => 
             let val datbind = loopDatBind datbind
             in DATATYPEDec(I, datbind) end
         | (DATATYPE2Dec(I, tycon, longtycon)) => DATATYPE2Dec(I, tycon, longtycon)
         | (ABSTYPEDec(I, datbind, dec)) => let
             val datbind = loopDatBind datbind
             val dec = loopDec dec
           in ABSTYPEDec(I, datbind, dec) end
         | (EXCEPTIONDec(I, exbind)) => 
             let val exbind = loopExBind exbind
             in EXCEPTIONDec(I, exbind) end
         | (LOCALDec(I, dec1, dec2)) => let
             val dec1 = loopDec dec1
             val dec2 = loopDec dec2
           in LOCALDec(I, dec1, dec2) end
         | (OPENDec(I, longstrids)) => OPENDec(I, longstrids)
         | (EMPTYDec(I)) => EMPTYDec(I)
         | (SEQDec(I, dec1, dec2)) => let
             val dec1 = loopDec dec1
             val dec2 = loopDec dec2
           in SEQDec(I, dec1, dec2) end


    and loopValBind (PLAINValBind(I, pat, exp, valbind_opt)) =
        let
          val pat = loopPat pat
          val tau = getType (infoPat pat)
          val tau' = getType (infoExp exp)
          val exp = if !(getLv tau) = Level.Stable
                    then loopExp (Level.Stable, exp)
                    else ExpApp(I, vidMod, PARAtExp(I,loopExp(Level.Changeable, exp))) (* TODO *)
          val valbind_opt = Option.map loopValBind valbind_opt
        in
          PLAINValBind(I, pat, exp, valbind_opt)
        end
      | loopValBind (RECValBind(I, valbind)) = RECValBind(I,loopValBind valbind)

    and loopTypBind (TypBind(I, tyvarseq, tycon, ty, typbind_opt)) =
          let val typbind_opt = Option.map loopTypBind typbind_opt
          in TypBind(I, tyvarseq, tycon, ty, typbind_opt) end
          
    and loopDatBind (DatBind(I, tyvarseq, tycon, lv, conbind, datbind_opt)) =
        let 
          val conbind = loopConBind conbind
          val datbind_opt = Option.map loopDatBind datbind_opt
        in
          DatBind(I, tyvarseq, tycon, lv, conbind, datbind_opt)
        end

    and loopConBind (ConBind(I, ops, vid, ty_opt, conbind_opt)) =
        let
          val ty_opt = Option.map loopTy ty_opt
          val conbind_opt = Option.map loopConBind conbind_opt
        in
          ConBind(I, ops, vid, ty_opt, conbind_opt)
        end

    and loopExBind (NEWExBind(I, ops, vid, ty_opt, exbind_opt)) = let
          val ty_opt = Option.map loopTy ty_opt
          val exbind_opt = Option.map loopExBind exbind_opt
        in NEWExBind(I, ops, vid, ty_opt, exbind_opt) end
      | loopExBind (EQUALExBind(I, ops1, vid, ops2, longvid, exbind_opt)) =
          let val exbind_opt = Option.map loopExBind exbind_opt
          in EQUALExBind(I, ops1, vid, ops2, longvid, exbind_opt) end

    (* Patterns *)

    and loopAtPat (atpat) =
        let 
          val tau = getType(infoAtPat atpat)
        in
          case atpat of
            (WILDCARDAtPat(I)) => atpat
          | (SCONAtPat(I, scon)) => atpat
          | (IDAtPat(I, ops, longvid)) => 
               (case peekRefer I of
                  NONE => atpat (* bind a variable *) 
                | SOME I' => (* constructor *) 
                    atpat
               )
          | (RECORDAtPat(I, patrow_opt)) => 
              let
                val patrow_opt = Option.map loopPatRow patrow_opt
              in
                RECORDAtPat(I, patrow_opt)
              end
          | (PARAtPat(I, pat)) => 
              let val pat = loopPat pat
              in PARAtPat(I,pat) end
        end
                                
    and loopPatRow (DOTSPatRow(I)) = DOTSPatRow(I)
      | loopPatRow (FIELDPatRow(I, lab, pat, patrow_opt)) = (* Fst *)
        let
          val pat = loopPat pat
          val patrow_opt = Option.map loopPatRow patrow_opt
        in FIELDPatRow(I, lab, pat, patrow_opt) end

    and loopPat pat =
        let 
          val tau = getType(infoPat pat)
        in
          case pat of 
            (ATPat(I, atpat)) => 
              let val atpat = loopAtPat atpat
              in ATPat(I, atpat) end
          | (CONPat(I, ops, longvid, atpat)) => (* TODO *)
              let
                val atpat = loopAtPat atpat
              in
                CONPat(I, ops, longvid, atpat)
              end
          | (COLONPat(I, pat, ty)) => 
              let
                val pat = loopPat pat
                val ty = loopTy ty
              in
                COLONPat(I, pat, ty)
              end
          | (ASPat(I, ops, vid, ty_opt, pat)) => 
              let
                val ty_opt = Option.map loopTy ty_opt
                val pat = loopPat pat
              in
                ASPat(I, ops, vid, ty_opt, pat)
              end
        end

    (* Type *)

    and loopTy ty = ty
(*
        case ty of
          VARTy(I,tyvar) => VARTy(I, tyvar)
        | RECORDTy(I,tyrow_opt,lv) => 
          let
            val tyrow_opt = Option.map loopTyRow tyrow_opt

        | CONTy(I, tyseq, longtycon, lv) => 
          let
            val tau = CONTy(I, tyseq, longtycon, Level.Unknown)
            val Tyseq(_,tys) = tyseq
            val tys = List.map loopTy tys
          in
            case lv of
              Level.Changeable => let
                val last = CONTy (I, [], longtycon, Level.Unknown)
                val tys = tys :: last
                in CONTy(I, tys, 

            | ARROWTy(I,ty,ty',mode,lv) => 
              let
                val tau1 = getASTTy ty
                val tau2 = getASTTy ty'
              in Type.fromFunType(tau1,tau2,ref mode,ref lv) end
            | PARTy(I,ty) => getASTTy ty
            and loopTyRow (TyRow(I,lab,ty,tyrow_opt)) =
                let
                  val tau = getASTTy ty
                  val rho = case tyrow_opt of
                              NONE => Type.emptyRow
                            | SOME tyrow => getTyRow tyrow
                in Type.insertRow(rho,lab,tau) end 
*)

    (* Module *)

    (* Structures *)

    fun loopStrDec (DECStrDec(I, dec)) = 
          let val dec = loopDec dec
          in DECStrDec(I, dec) end
      | loopStrDec (STRUCTUREStrDec(I, strbind)) = 
          let val strbind = loopStrBind strbind
          in STRUCTUREStrDec(I, strbind) end
      | loopStrDec (LOCALStrDec(I, strdec1, strdec2)) = let
          val strdec1 = loopStrDec strdec1
          val strdec2 = loopStrDec strdec2
          in LOCALStrDec(I, strdec1, strdec2) end
      | loopStrDec (EMPTYStrDec(I)) = EMPTYStrDec(I)
      | loopStrDec (SEQStrDec(I, strdec1, strdec2)) = let
          val strdec1 = loopStrDec strdec1
          val strdec2 = loopStrDec strdec2
          in SEQStrDec(I, strdec1, strdec2) end

    and loopStrBind (StrBind(I, strid, strexp, strbind_opt)) =
        let
          val strexp = loopStrExp strexp
          val strbind_opt = Option.map loopStrBind strbind_opt
        in
          StrBind(I, strid, strexp, strbind_opt)
        end

    and loopStrExp (STRUCTStrExp(I, strdec)) = STRUCTStrExp(I, loopStrDec strdec)
      | loopStrExp (IDStrExp(I, longstrid)) = IDStrExp(I, longstrid)
      | loopStrExp (COLONStrExp(I, strexp, sigexp)) = COLONStrExp(I, loopStrExp strexp, sigexp)
      | loopStrExp (SEALStrExp(I, strexp, sigexp)) = SEALStrExp(I, loopStrExp strexp, sigexp)
      | loopStrExp (APPStrExp(I, funid, strexp)) = APPStrExp(I, funid, loopStrExp strexp)
      | loopStrExp (LETStrExp(I, strdec, strexp)) = LETStrExp(I, loopStrDec strdec, loopStrExp strexp)

    (* Top-level declarations *)

    and loopTopDec (STRDECTopDec(I, strdec, topdec_opt)) = 
          STRDECTopDec(I, loopStrDec strdec, Option.map loopTopDec topdec_opt)
      | loopTopDec (SIGDECTopDec(I, sigdec, topdec_opt)) = 
          SIGDECTopDec(I, sigdec, Option.map loopTopDec topdec_opt)
      | loopTopDec d = d

    (* Programs *)

    fun loopProgram (Program(I, topdec, program_opt)) =
        Program(I, loopTopDec topdec, Option.map loopProgram program_opt)

    fun transProgram program =
       loopProgram program

end;
