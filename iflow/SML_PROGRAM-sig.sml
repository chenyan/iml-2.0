(*
 * (c) Andreas Rossberg 2007
 *
 * Printer for abstract program grammar
 *)

signature SML_PROGRAM =
sig
    type Program = SyntaxProgram.Program
    type doc = PrettyPrint.doc

    val dispType : bool ref

    val printSML : Program -> unit
    val ppExp : SyntaxCore.Exp -> doc
    val ppAtExp : SyntaxCore.AtExp -> doc
end;
