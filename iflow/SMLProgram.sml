
structure SMLProgram : SML_PROGRAM =
struct
    (* Import *)

    open SyntaxProgram
    open SyntaxModule
    open SyntaxCore
    open Annotation
    open AnnotationProgram
    open PrettyPrint
    open PPMisc

    infixr ^^ ^/^

    val dispType = ref true

    fun ppOpt ppX NONE = empty
      | ppOpt ppX (SOME x) = ppX x

    (* Type Environment *)

    val red = str(chr(27))^"[31m"
    val purple = str(chr(27))^"[35m"
    val blue = str(chr(27))^"[34m"
    val black = str(chr(27))^"[0m"
(*
    val getType = StaticObjectsCore.getType
    val getScheme = StaticObjectsCore.getScheme
    fun showType I = if !dispType then
      let 
        val refer = (*case StaticObjectsCore.peekGeneral I of
                        SOME sigma => 
                          let val sigma = (#1 sigma, LVType.getReferTy I)
                          in 
                            hbox(text blue ^^ PPType.ppTypeScheme sigma ^^ text black)
                          end
                      | NONE => *)empty
        val lsigma = case StaticObjectsCore.getLsigma I of
                       ([],_) => empty
                     | (vars,cons) => hbox(text purple ^^ text (LExp.lexpsToString (List.map LExp.Var vars)) ^/^ text ("["^Constraint.toString cons^"]") ^^ text black)
      in lsigma ^^ refer ^^ hbox(text red ^^ PPType.ppTypeScheme (getScheme I) ^^ text black)
      end
     else empty
*)

    fun showType A = 
      if !dispType then
        hbox(text red ^^ PPType.ppType (get (elab A)) ^^ text black)
      else empty

  (* Precedences *)

    val topPrec    = 0
    val funPrec    = topPrec + 1
    val asnPrec    = funPrec + 1
    val ifPrec     = asnPrec + 1
    val orPrec     = ifPrec + 1
    val andPrec    = orPrec + 1
    val bitorPrec  = andPrec + 1
    val bitxorPrec = bitorPrec + 1
    val bitandPrec = bitxorPrec + 1
    val eqPrec     = bitandPrec + 1
    val relPrec    = eqPrec + 1
    val shiftPrec  = relPrec + 1
    val addPrec    = shiftPrec + 1
    val mulPrec    = addPrec + 1
    val unPrec     = mulPrec + 1
    val newPrec    = unPrec + 1
    val postPrec   = newPrec + 1
    val atomPrec   = postPrec + 1

    (* Identifiers *)

    fun ppSigId (sigid@@_) = text(SigId.toString sigid)
    fun ppFunId (funid@@_) = text(FunId.toString funid)
    fun ppSCon (scon@@_)   = text(SCon.toString scon)
    fun ppLab (lab@@_)     = text(Lab.toString lab)
    fun ppVId (vid@@_)     = text(VId.toString vid) (*^/^ tyVid vid*)
    fun ppTyVar (tyvar@@_) = text(TyVar.toString tyvar)
    fun ppTyCon (tycon@@_) = text(TyCon.toString tycon)
    fun ppStrId (strid@@_) = text(StrId.toString strid)
    fun ppLvVar (lvvar@@_) = text(LvVar.toString lvvar)
    fun ppLv lv       = if !dispType then text(Level.toString lv) else empty

    fun ppLongVId (longvid@@_) = text(LongVId.toString longvid) (*^/^ tyLongVid longvid*)
    fun ppLongTyCon (longtycon@@_) = text(LongTyCon.toString longtycon)
    fun ppLongStrId (longstrid@@_) = text(LongStrId.toString longstrid)

    (* isTuple for label *)

    fun isTupleLab (lab@@_) = 
      case Lab.compare (lab, Lab.fromString "A") of
        LESS => true
      | _ => false
    fun isTuple (SOME (ExpRow(lab,_,_)@@_)) = isTupleLab lab
      | isTuple NONE = true
    fun isPatTuple (SOME (FIELDPatRow(lab, _, patrow_opt)@@_)) = isTupleLab lab
      | isPatTuple (SOME _) = false
      | isPatTuple NONE = true
    fun isTyTuple (SOME (TyRow(lab,_,_)@@_)) = isTupleLab lab
      | isTyTuple NONE = true

(*
    fun isTupleLab x = false
    fun isTuple x = false
    fun isPatTuple x = false
    fun isTyTuple x = false
*)
    (* Core *)

   (* Expressions *)

    fun ppAtExp (SCONAtExp(scon)@@A) = ppSCon scon ^/^ showType A
      | ppAtExp (IDAtExp(_, longvid)@@A) = ppLongVId longvid ^/^ showType A
      | ppAtExp (RECORDAtExp(exprow_opt)@@A) = 
          (if isTuple(exprow_opt) then
            hbox(paren(ppOpt ppExpRow exprow_opt))
          else hbox(brace(ppOpt ppExpRow exprow_opt)))
      | ppAtExp (LETAtExp(dec, exp)@@A) =
          vbox(nest(break ^^ text "let" ^^ 
                 nest(break ^^ below(ppDec dec)) ^/^
               text "in" ^^
                 nest(break ^^ below(nest(ppExp exp))) ^/^
               text "end"))
      | ppAtExp (PARAtExp(exp)@@A) = paren(ppExp exp)

    and ppExpRow (ExpRow(lab, exp, exprow_opt)@@A) =
          hbox(
          (if (isTupleLab lab) then empty
          else ppLab lab ^/^ text "=") ^/^
          ppExp exp ^/^ 
          ppOpt (fn x => text "," ^/^ ppExpRow x) exprow_opt)

    and ppExp (ATExp(atexp)@@A) = hbox(ppAtExp atexp)
      | ppExp (APPExp(exp, atexp)@@A) = hbox(ppExp exp ^/^ ppAtExp atexp) (*^/^ showType I*)
      | ppExp (COLONExp(exp, ty)@@A) = hbox(ppExp exp ^/^ text ":" ^/^ ppTy ty)
      | ppExp (HANDLEExp(exp, match)@@A) = 
          hbox(ppExp exp ^/^ text "handle" ^/^ ppMatch match)
      | ppExp (RAISEExp(exp)@@A) = hbox(text "raise" ^/^ ppExp exp)
      | ppExp (FNExp(match)@@A) = hbox(text "fn" ^/^ ppMatch match)

    (* Matches *)

    and ppMatch (Match(mrule, match_opt)@@A) = 
          vbox(ppMrule mrule ^/^ 
          ppOpt (fn x => hbox(text "|" ^/^ nest(ppMatch x))) match_opt)
    and ppMrule (Mrule(pat, exp)@@A) = 
          abox(hbox(ppPat pat ^/^ text "=>") ^/^
               nest(hbox(ppExp exp)))

    (* Declarations *)

    and ppDec (VALDec(tyvarseq, valbind)@@A) = 
          hbox(text "val" ^/^ ppTyVarseq tyvarseq ^/^ ppValBind valbind)
      | ppDec (TYPEDec(typbind)@@A) =
          hbox(text "type" ^/^ ppTypBind typbind)
      | ppDec (DATATYPEDec(datbind)@@A) =
          hbox(text "datatype" ^/^ ppDatBind datbind)
      | ppDec (DATATYPE2Dec(tycon, longtycon)@@A) =
          hbox(text "datatype" ^/^ ppTyCon tycon ^/^ text "=" ^/^ ppLongTyCon longtycon)
      | ppDec (ABSTYPEDec(datbind, dec)@@A) =
          hbox(text "abstype" ^/^ ppDatBind datbind ^/^ text "with" ^/^
          ppDec dec ^/^ text "end")
      | ppDec (EXCEPTIONDec(exbind)@@A) =
          hbox(text "exception" ^/^ ppExBind exbind)
      | ppDec (LOCALDec(dec1, dec2)@@A) =
          fbox(text "local" ^/^ 
               nest(ppDec dec1) ^/^ text "in" ^/^
               nest(ppDec dec2) ^/^ text "end")
      | ppDec (OPENDec(longstrids)@@A) = 
          hbox(text "open" ^/^ ppCommaList ppLongStrId longstrids)
      | ppDec (EMPTYDec@@A) = empty
      | ppDec (SEQDec(dec1, dec2)@@A) =
          vbox(hbox(ppDec dec1 ^^ text ";") ^/^ ppDec dec2)

    and ppValBind (PLAINValBind(pat, exp, valbind_opt)@@A) =
          vbox(hbox(ppPat pat ^/^ text "=" ^/^ ppExp exp) ^/^ 
               ppOpt (fn x => hbox(text "and" ^/^ ppValBind x)) valbind_opt)
      | ppValBind (RECValBind(valbind)@@A) = text "rec" ^/^ ppValBind valbind

    and ppTypBind (TypBind(tyvarseq, tycon, ty, typbind_opt)@@A) =
          vbox(hbox(ppTyVarseq tyvarseq ^/^ ppTyCon tycon ^/^ text "=" ^/^ ppTy ty) ^/^ 
               ppOpt (fn x => hbox(text "and" ^/^ ppTypBind x)) typbind_opt)
          
    and ppDatBind (DatBind(tyvarseq, tycon, lv, conbind, datbind_opt)@@A) =
          vbox(hbox(ppTyVarseq tyvarseq ^/^ ppTyCon tycon ^/^ below(text "=" ^/^ ppLv lv ^/^ ppConBind conbind)) ^/^
          ppOpt (fn x => hbox(text "and" ^/^ ppDatBind x)) datbind_opt)

    and ppConBind (ConBind(_, vid, ty_opt, conbind_opt)@@A) =
          vbox(hbox(ppVId vid ^/^ ppOpt (fn doc => text "of" ^/^ ppTy doc) ty_opt) ^/^
               ppOpt (fn x => hbox(text "|" ^/^ ppConBind x)) conbind_opt)

    and ppExBind (NEWExBind(_, vid, ty_opt, exbind_opt)@@A) =
          vbox(hbox(ppVId vid ^/^ ppOpt (fn doc => text "of" ^/^ ppTy doc) ty_opt) ^/^
          ppOpt (fn x => hbox(text "and" ^/^ ppExBind x)) exbind_opt)
      | ppExBind (EQUALExBind(_, vid, _, longvid, exbind_opt)@@A) =
          vbox(hbox(ppVId vid ^/^ text "=" ^/^ ppLongVId longvid) ^/^ 
               ppOpt (fn x => hbox(text "and" ^/^ ppExBind x)) exbind_opt)

    (* Patterns *)

    and ppAtPat (WILDCARDAtPat@@A) = text "_"
      | ppAtPat (SCONAtPat(scon)@@A) = ppSCon scon
      | ppAtPat (IDAtPat(_, longvid)@@A) = ppLongVId longvid
      | ppAtPat (RECORDAtPat(patrow_opt)@@A) = 
          hbox(if isPatTuple patrow_opt then
            paren(ppOpt ppPatRow patrow_opt)
          else brace(ppOpt ppPatRow patrow_opt))
      | ppAtPat (PARAtPat(pat)@@A) = paren(ppPat pat)

    and ppPatRow (DOTSPatRow@@A) = text "..."
      | ppPatRow (FIELDPatRow(lab, pat, patrow_opt)@@A) =
          hbox((if isTupleLab lab then empty
          else ppLab lab ^/^ text "=") ^/^
          ppPat pat ^/^ 
          ppOpt (fn x => text "," ^/^ ppPatRow x) patrow_opt)

    and ppPat (ATPat(atpat)@@A) = ppAtPat atpat
      | ppPat (CONPat(_, longvid, atpat)@@A) = ppLongVId longvid ^/^ ppAtPat atpat
      | ppPat (COLONPat(pat, ty)@@A) = ppPat pat ^/^ text ":" ^/^ ppTy ty
      | ppPat (ASPat(_, vid, ty_opt, pat)@@A) = 
          ppVId vid ^/^ ppOpt (fn x => text ":" ^/^ ppTy x) ty_opt ^/^
          text "as" ^/^ ppPat pat

    (* Type expressions *)

    and ppTy (VARTy(tyvar)@@A) = ppTyVar tyvar
      | ppTy (RECORDTy(tyrow_opt, level)@@A) = 
          if (isTyTuple tyrow_opt) then
            paren(ppOpt ppTyRow tyrow_opt) ^/^ ppLv level
          else brace(ppOpt ppTyRow tyrow_opt) ^/^ ppLv level
      | ppTy (CONTy(tyseq, longtycon, level)@@A) =
          ppTyseq tyseq ^/^ ppLongTyCon longtycon ^/^ ppLv level
      | ppTy (ARROWTy(ty1, ty2, mode, level)@@A) =
          paren(ppTy ty1 ^/^ text "->" ^/^ ppLv mode ^/^ ppTy ty2) ^/^ ppLv level
      | ppTy (PARTy(ty)@@A) = paren(ppTy ty)

    and ppTyRow (TyRow(lab, ty, tyrow_opt)@@A) =
          hbox(if (isTupleLab lab) then
            ppTy ty ^/^ ppOpt (fn x => text "*" ^/^ ppTyRow x) tyrow_opt
          else ppLab lab ^/^ text ":" ^/^ ppTy ty ^/^
               ppOpt (fn x => text "," ^/^ ppTyRow x) tyrow_opt)

    and ppTyseq (Seq(tys)@@A) = ppCommaList ppTy tys

    and ppTyVarseq (Seq(tyvars)@@A) = ppCommaList ppTyVar tyvars


    (* Module *)

    (* Structures *)

    fun ppStrDec (DECStrDec(dec)@@A) = 
          hbox(ppDec dec)
      | ppStrDec (STRUCTUREStrDec(strbind)@@A) =
          hbox(text "structure" ^/^ ppStrBind strbind)
      | ppStrDec (LOCALStrDec(strdec1, strdec2)@@A) =
          vbox(nest(break ^^ text "local" ^^
                 nest(break ^^ below(ppStrDec strdec1)) ^/^
               text "in" ^/^
                 nest(break ^^ below(ppStrDec strdec2)) ^/^
               text "end"))
      | ppStrDec (EMPTYStrDec@@A) = empty
      | ppStrDec (SEQStrDec(strdec1, strdec2)@@A) =
          vbox(hbox(ppStrDec strdec1 ^^ text";") ^/^ ppStrDec strdec2)

    and ppStrBind (StrBind(strid, strexp, strbind_opt)@@A) =
          vbox(hbox(ppStrId strid ^/^ text "=") ^/^ 
                  nest(break ^^ ppStrExp strexp) ^/^
               ppOpt (fn x => text "and" ^/^ ppStrBind x) strbind_opt)

    and ppStrExp (STRUCTStrExp(strdec)@@A) = 
          vbox(nest(text "struct" ^/^ below(ppStrDec strdec)) ^/^ text "end")
      | ppStrExp (IDStrExp(longstrid)@@A) = ppLongStrId longstrid
      | ppStrExp (COLONStrExp(strexp, sigexp)@@A) = hbox(ppStrExp strexp ^/^ text ":" ^/^ ppSigExp sigexp)
      | ppStrExp (SEALStrExp(strexp, sigexp)@@A) = hbox(ppStrExp strexp ^/^ text ":>" ^/^ ppSigExp sigexp)
      | ppStrExp (APPStrExp(funid, strexp)@@A) = ppFunId funid ^/^ paren(ppStrExp strexp)
      | ppStrExp (LETStrExp(strdec, strexp)@@A) = 
          vbox(nest(break ^^ text "let" ^^
                 nest(break ^^ below(ppStrDec strdec)) ^/^
               text "in" ^/^
                 nest(break ^^ below(ppStrExp strexp)) ^/^
               text "end"))

    (* Signatures *)

    and ppSigExp (SIGSigExp(spec)@@A) = vbox(nest(text "sig" ^/^ below(ppSpec spec)) ^/^ text "end")
      | ppSigExp (IDSigExp(sigid)@@A) = ppSigId sigid
      | ppSigExp (WHERETYPESigExp(sigexp, tyvarseq, longtycon, ty)@@A) = 
          vbox(ppSigExp sigexp ^/^
               hbox(text "where type" ^/^ ppTyVarseq tyvarseq ^/^ ppLongTyCon longtycon ^/^ text "=" ^/^ ppTy ty))

    and ppSigDec (SigDec(sigbind)@@A) = hbox(text "signature" ^/^ ppSigBind sigbind)

    and ppSigBind (SigBind(sigid, sigexp, sigbind_opt)@@A) = 
          vbox(hbox(ppSigId sigid ^/^ text "=" ^/^ ppSigExp sigexp) ^/^
               ppOpt (fn x => hbox (text "and" ^/^ ppSigBind x)) sigbind_opt)

    (* TODO: Specification *)

    and ppSpec x = empty


    (* Top-level declarations *)

    and ppTopDec (STRDECTopDec(strdec, topdec_opt)@@A) =
          vbox(ppStrDec strdec ^/^
               ppOpt (fn x => ppTopDec x) topdec_opt)
      | ppTopDec (SIGDECTopDec(sigdec, topdec_opt)@@A) =
          vbox(ppSigDec sigdec ^/^
               ppOpt (fn x => ppTopDec x) topdec_opt)
      | ppTopDec _ = empty


    (* Programs *)

    fun ppProgram (Program(topdec, program_opt)@@A) =
      vbox(
        ppTopDec topdec ^/^
        ppOpt ppProgram program_opt ^/^
        text ""
      )
(*
    fun printSML (Basis, program) =
        ( B := Basis;
          PrettyPrint.output(TextIO.stdOut, ppProgram program, 79);
          TextIO.flushOut TextIO.stdOut
        )
*)
    fun printSML program = (PrettyPrint.output(TextIO.stdOut, ppProgram program, 79);
                            TextIO.flushOut TextIO.stdOut)

end;
